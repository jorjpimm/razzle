#include "razzle/razzle.h"
//#include "razzle/utils.h"

#include "catch2/catch.hpp"

class MemoryIO : public razzle::IOInterface {
    std::vector<gsl::byte> data;
    
    tl::expected<void, razzle::Error> write_bytes(std::uint64_t location, gsl::span<gsl::byte const> in_data) {
        auto const required_size = location + in_data.size() + 1;
        if (data.size() < required_size) {
            data.resize(required_size);
        }

        std::copy(in_data.begin(), in_data.end(), data.data() + location);
        return {};
    }

    tl::expected<void, razzle::Error> read_bytes(std::uint64_t location, gsl::span<gsl::byte> out_data) {
        auto const required_size = location + out_data.size() + 1;
        if (data.size() < required_size) {
            return tl::make_unexpected<razzle::Error>({});
        }

        std::copy(data.data() + location, data.data() + location + out_data.size(), out_data.begin());
        return {};
    }
};

TEST_CASE("razzle api opening a file") {
    using namespace razzle;

    std::shared_ptr<razzle::IOInterface> file_data;
    {
        File file{ std::make_shared<MemoryIO>(), File::CreateMode::New };
        CHECK(file.is_open());

        file_data = file.close_and_release();
    }

    {
        File file{ file_data, File::OpenMode::ReadOnly };
        CHECK(file.is_open());
    }

    CHECK_THROWS(File{ std::make_shared<MemoryIO>(), File::OpenMode::ReadOnly });
    CHECK_THROWS(File{ std::make_shared<MemoryIO>(), File::OpenMode::ReadWrite });
}

#if 0
TEST_CASE("razzle api") {
    using namespace razzle;

    std::shared_ptr<razzle::IOInterface> file_data;
    {
        File file{ std::make_shared<MemoryIO>(), File::CreateMode::New };
        CHECK(file.is_open());

        auto root = file.root_group().value();

        auto foo = root.add_group("foo");
        foo.add_group("attr1");
        
        auto bar = foo.add_group("bar");    
        bar.add_group("attr2");
        foo.add_group("attr3");

        file_data = file.close_and_release();
        CHECK(!file.is_open());
    }

    std::string dumped = utils::describe_file(std::move(file_data));
}
#endif