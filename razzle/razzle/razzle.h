#pragma once

#include <tl/expected.hpp>

#include <gsl/gsl>

#include <memory>
#include <string>

namespace razzle {
    
class Object;
class Attribute;

class Error {
public:
    std::string describe() const { return {}; }
};

class Exception : std::exception {
public:
    Exception(Error const& error) : m_error(error) {}

    Error error() { return m_error; }

    const char* what() const noexcept override {
        if (m_str.empty()) {
            m_str = "Error: " + m_error.describe();
        }
        return m_str.c_str();
    }

private:
    Error m_error;
    mutable std::string m_str;
};

/// Wrapper around IO surface.
class IOInterface {
public:
    virtual ~IOInterface() = default;

    virtual tl::expected<void, Error> write_bytes(std::uint64_t location, gsl::span<gsl::byte const> in_data) = 0;
    
    template <typename T>
    tl::expected<void, Error> write(std::uint64_t location, T const& t) {
        return write_bytes(location, gsl::as_bytes(gsl::make_span(&t, 1)));
    }

    virtual tl::expected<void, Error> read_bytes(std::uint64_t location, gsl::span<gsl::byte> in_data) = 0;
    
    template <typename T>
    tl::expected<void, Error> read(std::uint64_t location, T& t) {
        return read_bytes(location, gsl::as_writable_bytes(gsl::make_span(&t, 1)));
    }
};

class Name {
public:
    std::string string() const;
};

class SentinelIterator {};

/// A list of attributes belonging to an object.
class AttributeList {
public:
    class AttributeIterator {
    public:
        bool operator!=(SentinelIterator) const;
        Attribute operator*() const;
        void operator++();
    };

    AttributeIterator begin();
    SentinelIterator end() { return {}; }
};

/// A list of children belonging to a group.
class ChildList {
public:
    class ChildIterator {
    public:
        bool operator!=(SentinelIterator) const;
        Object operator*() const;
        void operator++();
    };

    ChildIterator begin();
    SentinelIterator end() { return {}; }
};


class Object {
public:
    // Name of this object
    Name name() const;
    // Full path of this object in the file, in the form: <grandparent>/<parent>/<name>
    std::string full_path() const;

    // List of attribute children for this object.
    AttributeList attributes() const;

    // Add a new attribute named [name].
    Attribute add_attribute(std::string const& name);
};

class Attribute : public Object {
public:
    // ...
};

class Group : public Object {
public:
    ChildList children() const;

    // Add a new group called [name].
    Group add_group(std::string const& name);
};

class Dataset : public Object {
public:
    // ...
};

/// Write data in razzle form to the passed IOInterface.
class File {
public:
    enum class CreateMode {
        New
    };

    enum class OpenMode {
        ReadOnly,
        ReadWrite
    };

    // Open a new file, potentially truncating an existing file.
    // Note: Throws if the file cannot be opened in the requested mode.
    File(std::shared_ptr<IOInterface> io_interface, CreateMode mode);
    // Open an existing file in [mode] (ie as read only, or read/write).
    // Note: Throws if the file cannot be opened in the requested mode.
    File(std::shared_ptr<IOInterface> io_interface, OpenMode mode);

    /// Find if the file is currently open, and ready for reads/writes.
    bool is_open() const;

    // Close the file, clearing the io interface, causing no
    // further interaction through this class to be possible.
    std::shared_ptr<IOInterface> close_and_release();

    // Find the root group for the file, or an error if the file isn't open.
    tl::expected<Group, Error> root_group();

private:
    std::shared_ptr<IOInterface> m_io_interface;
};

}