#include "razzle/razzle.h"
#include "razzle/internal/disk_format.h"
#include "razzle/internal/disk_format_latest.h"

namespace razzle {

File::File(std::shared_ptr<IOInterface> io_interface, CreateMode mode)
: m_io_interface(io_interface)
{
    assert(mode == CreateMode::New);

    disk_format_latest::FileHeader header;
    header.magic_bytes = disk_format::MAGIC_BYTES;
    header.version = disk_format_latest::VERSION;
    header.root_group = decltype(header.root_group)::from(sizeof(header));

    disk_format_latest::Group root_group;

    auto header_write_result = io_interface->write(0, header);
    if (!header_write_result) {
        throw header_write_result.error();
    }

    auto group_write_result = io_interface->write(header.root_group.byte_location(), root_group);
    if (!group_write_result) {
        throw group_write_result.error();
    }
}

File::File(std::shared_ptr<IOInterface> io_interface, OpenMode mode)
: m_io_interface(io_interface)
{
    disk_format::BaseFileHeader header;
    auto header_read_result = io_interface->read(0, header);
    if (!header_read_result) {
        throw header_read_result.error();
    }

    if (header.magic_bytes != disk_format::MAGIC_BYTES) {
        throw Exception{{}};
    }

    if (header.version != disk_format_latest::VERSION) {
        assert(false && "should be able to do this...");
        throw Exception{{}};
    }
}

bool File::is_open() const {
    return !!m_io_interface;
}

std::shared_ptr<IOInterface> File::close_and_release() {
    std::shared_ptr<IOInterface> io_interface;
    std::swap(m_io_interface, io_interface);
    return io_interface;
}

tl::expected<Group, Error> File::root_group() {
    if (!m_io_interface) {
        return tl::make_unexpected<Error>({});
    }

    return Group{ };
}

}