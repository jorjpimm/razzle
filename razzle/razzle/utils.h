#pragma once

#include <cstdint>
#include <string>

namespace razzle { namespace utils {

void describe_group(
    Group const& group,
    std::string& out,
    std::uint32_t indent
) {
    std::string output;

    output += group.name().string() + "\n";
    for(auto attr : group.attributes()) {
        output += attr.name().string() + "\n";
    }

    for(auto child : group.children()) {
        output += child.name().string() + "\n";
    }
}

std::string describe_file(std::shared_ptr<razzle::IOInterface> const& io_interface) {
    File file{ io_interface, File::OpenMode::ReadOnly };

    auto group_result = file.root_group();
    if (!group_result) {
        return "Error opening root group: " + group_result.error().describe();
    }

    std::string output;
    describe_group(group_result.value(), output, 0);
    return output;
}
}}