#pragma once

#include "razzle/internal/disk_format_v1.h"

namespace razzle {

namespace disk_format_latest = disk_format_v1;

}