#pragma once

#include "razzle/internal/disk_format.h"

#include <array>
#include <cstdint>

namespace razzle { namespace disk_format_v1 {

// Absolute location of an object in a given file.
template <typename T, typename OffsetType=std::uint64_t>
class ObjectOffset {
public:
    static ObjectOffset from(OffsetType offset) {
        ObjectOffset obj;
        obj.m_offset = offset;
        return obj;
    }

    OffsetType byte_location() const { return m_offset; }

private:
    OffsetType m_offset;
};

struct ShortByteArray {
    std::uint16_t length;
    char bytes;
};

using NameString = ShortByteArray;

struct Object;
struct Attribute;

struct AttributeContents {
    std::array<ObjectOffset<Attribute>, 16> contents;
    ObjectOffset<AttributeContents> next_attributes;
};

struct GroupContents {
    std::array<ObjectOffset<Object>, 16> contents;
    ObjectOffset<GroupContents> next_contents;
};

struct Object {
    ObjectOffset<NameString> name;
    AttributeContents primary_attributes;
};


struct Attribute {
    Object object;
};

struct Dataset {
    Object object;
};

struct Group {
    Object object;
    GroupContents primary_contents;
};

struct FileHeader : disk_format::BaseFileHeader {
    ObjectOffset<Group, std::uint8_t> root_group;
};

static constexpr std::uint16_t VERSION = 1;

}}