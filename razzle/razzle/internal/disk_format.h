#pragma once

namespace razzle {

namespace disk_format {
 
struct BaseFileHeader {
    std::uint32_t magic_bytes;
    std::uint16_t version;
};

static constexpr std::uint32_t MAGIC_BYTES = 'razz';

}
}